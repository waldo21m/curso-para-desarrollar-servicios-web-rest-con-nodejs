const express = require("express");
const favoritesController = require("../controllers/FavoritesController");
const placesController = require("../controllers/PlacesController");
const authenticateOwner = require("../middlewares/authenticateOwner");

const router = express.Router();

router
  .route("/:id/favorites")
  .get(placesController.find, favoritesController.index);

router
  .route("/:id/favorites/:favorite_id")
  .delete(favoritesController.find, authenticateOwner, favoritesController.destroy);

module.exports = router;
