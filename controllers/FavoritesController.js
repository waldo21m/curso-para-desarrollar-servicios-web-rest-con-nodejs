const favoritePlace = require("../models/favoritePlace");
const buildParams = require("./helpers").buildParams;

const validParams = ["_place"];

function find(req, res, next) {
  favoritePlace
    .findById(req.params.favorite_id)
    .then((fav) => {
      req.mainObj = fav;
      req.favorite = fav;
      next();
    })
    .catch(next);
}

function index(req, res) {
  let promise = null;

  if (req.place) {
    promise = req.place.favorites;
  } else if (req.fullUser) {
    promise = req.fullUser.favorites;
  } else {
    res.json({});
  }

  if (promise) {
    promise
      .then((favorites) => {
        res.json(favorites);
      })
      .catch((error) => {
        res.status(500).json({ error });
      });
  } else {
    res.status(404).json({});
  }
}

function create(req, res) {
  const params = buildParams(validParams, req.body);
  params["_user"] = req.user.id;
  favoritePlace
    .create(params)
    .then((favorite) => {
      res.json(favorite);
    })
    .catch((error) => {
      res.status(422).json({ error });
    });
}

function destroy(req, res) {
  req.favorite
    .remove()
    .then(() => {
      res.json({});
    })
    .catch((error) => {
      res.status(500).json({ error });
    });
}

module.exports = {
  find,
  index,
  create,
  destroy,
};
