function buildParams(validParams, body) {
  /**
   * Este algoritmo lo que hace es verificar si estos atributos existen en el
   * req.body. Los atributos que si existan, serán almacenados en un objeto
   * json params que usaremos en el proyecto.
   */
  let params = {};

  validParams.forEach((attr) => {
    if (Object.prototype.hasOwnProperty.call(body, attr)) {
      // En un objeto JSON, req.body.title === req.body['title']
      params[attr] = body[attr];
    }
  });

  return params;
}

module.exports = { buildParams };
