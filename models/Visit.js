const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");

const REACTIONS = [
  "like",
  "love",
  "disappointment",
  "yummy",
  "anger",
  "disgust",
];

let visitSchema = new mongoose.Schema({
  _user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  _place: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Place",
    required: true,
  },
  reaction: {
    type: String,
    enum: REACTIONS,
  },
  observation: String,
});

visitSchema.statics.forUser = function (userId, page) {
  return Visit.paginate(
    { _user: userId },
    {
      page: page,
      limit: 5,
      sort: { _id: -1 },
      populate: [
        { path: "_user", select: ["email", "name"] },
        {
          path: "_place",
          select: [
            "title",
            "address",
            "description",
            "openHour",
            "closeHour",
            "acceptsCreditCard",
            "slug",
            "createdAt",
            "updatedAt",
          ],
        },
      ],
    }
  );
};

visitSchema.plugin(mongoosePaginate);

const Visit = mongoose.model("Visit", visitSchema);

module.exports = Visit;
