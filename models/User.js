const mongoose = require("mongoose");
const mongooseBcrypt = require("mongoose-bcrypt");

let userSchema = new mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  name: String,
  admin: {
    type: Boolean,
    default: false,
  },
});

userSchema.virtual("places").get(function () {
  return Place.find({ _user: this._id });
});

userSchema.virtual("favorites").get(function () {
  return favoritePlace.find({ _user: this._id }, { _place: true }).populate([
    { path: "_user", select: ["email", "name"] },
    {
      path: "_place",
      select: [
        "title",
        "address",
        "description",
        "openHour",
        "closeHour",
        "acceptsCreditCard",
        "createdAt",
        "updatedAt",
      ],
    },
  ]);
});

userSchema.post("save", function (user, next) {
  User.count({}).then((count) => {
    if (count == 1) {
      // No podemos hacer esto porque es un búcle infinito
      // user.admin = true;
      // user.save().then(next);

      User.update({ _id: user._id }, { admin: true }).then(() => {
        next();
      });
    } else {
      next();
    }
  });
});

userSchema.plugin(mongooseBcrypt);

const User = mongoose.model("User", userSchema);

module.exports = User;

const Place = require("./Place");
const favoritePlace = require("./favoritePlace");
