const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const uploader = require("./Uploader");
const slugify = require("../plugins/slugify");

let placeSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    slug: {
      type: String,
      unique: true,
    },
    address: String,
    description: String,
    acceptsCreditCard: {
      type: Boolean,
      default: false,
    },
    coverImage: String,
    avatarImage: String,
    openHour: Number,
    closeHour: Number,
    _user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
  },
  { timestamps: true }
);

placeSchema.virtual("favorites").get(function () {
  return favoritePlace.find({ _place: this._id }, { _user: true }).populate([
    { path: "_user", select: ["email", "name"] },
    {
      path: "_place",
      select: [
        "title",
        "address",
        "description",
        "openHour",
        "closeHour",
        "acceptsCreditCard",
        "createdAt",
        "updatedAt",
      ],
    },
  ]);
});

placeSchema.methods.updateImage = function (path, imageType) {
  // Primero subir la imágen
  // Guardar el lugar

  // Encadenar promesas
  return uploader(path).then((secure_url) =>
    this.saveImageUrl(secure_url, imageType)
  );
};

placeSchema.methods.saveImageUrl = function (secureUrl, imageType) {
  this[imageType + "Image"] = secureUrl;
  return this.save();
};

placeSchema.pre("save", function (next) {
  if (this.slug) return next();
  generateSlugAndContinue.call(this, 0, next);
});

placeSchema.statics.validateSlugCount = function (slug) {
  return Place.count({ slug: slug }).then((count) => {
    if (count > 0) return false;
    return true;
  });
};

function generateSlugAndContinue(count, next) {
  this.slug = slugify(this.title);

  if (count != 0) this.slug = this.slug + "-" + count;

  Place.validateSlugCount(this.slug).then((isValid) => {
    if (!isValid) {
      return generateSlugAndContinue.call(this, count + 1, next);
    }

    next();
  });
}

placeSchema.virtual("visits").get(function () {
  return Visit.find({ _place: this._id })
    .sort({ _id: -1 })
    .populate([
      { path: "_user", select: ["email", "name"] },
      {
        path: "_place",
        select: [
          "title",
          "address",
          "description",
          "openHour",
          "closeHour",
          "acceptsCreditCard",
          "createdAt",
          "updatedAt",
        ],
      },
    ]);
});

placeSchema.plugin(mongoosePaginate);

const Place = mongoose.model("Place", placeSchema);

module.exports = Place;

const favoritePlace = require("./favoritePlace");
const Visit = require("./Visit");
