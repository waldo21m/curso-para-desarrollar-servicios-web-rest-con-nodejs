require('dotenv').config();

module.exports = {
    cloudinary: {
        api_key: process.env.CLOUDINARY_API_KEY,
        cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
        api_secret: process.env.CLOUDINARY_API_SECRET
    },
    jwtSecret: process.env.JWT_SECRET
};