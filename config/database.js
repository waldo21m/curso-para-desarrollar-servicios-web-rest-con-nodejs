const mongoose = require("mongoose");
require("dotenv").config();

const dbHost = process.env.DB_HOST;
const dbPort = process.env.DB_PORT;
const dbName = process.env.DB_DATABASE;

module.exports = {
  connect: () =>
    mongoose.connect(`mongodb://${dbHost}:${dbPort}/${dbName}`, {
      useNewUrlParser: true,
      useCreateIndex: true,
    }),
  dbName,
  connection: () => {
    if (mongoose.connection) {
      return mongoose.connection;
    }

    return this.connect();
  },
};
