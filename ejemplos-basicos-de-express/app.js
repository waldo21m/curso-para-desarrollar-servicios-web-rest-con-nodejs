const express = require("express");
const bodyParser = require('body-parser');
const app = express();

const places = [
    {
        title: "Oficina CódigoFacilito",
        description: "Lorem Ipsum",
        address: "Lorem Ipsum"
    },
    {
        title: "Oficina CódigoFacilito",
        description: "Lorem Ipsum",
        address: "Lorem Ipsum"
    },
    {
        title: "Oficina CódigoFacilito",
        description: "Lorem Ipsum",
        address: "Lorem Ipsum"
    },
];

app.use(bodyParser.json({}));
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/', (req, res) => {
    res.json(places);
});

app.post('/', (req, res) => {
    res.json(req.body);
});

app.use(express.static('public'));

app.listen(3000, function () {
    console.log("Estoy listo para recibir peticiones");
});