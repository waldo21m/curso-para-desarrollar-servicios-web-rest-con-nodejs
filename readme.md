# Curso para desarrollar servicios web REST con Nodejs

## Lecciones:

* [Introducción](#module01)
    * [Lección #1 - Introducción al proyecto del curso](#lesson01)
    * [Lección #2 - Requisitos previos para tomar este curso](#lesson02)
    * [Lección #3 - Qué cubriremos en este curso](#lesson03)
    * [Lección #4 - Descripción del proyecto](#lesson04)
    * [Lección #5 - Configurar entorno de trabajo (Windows)](#lesson05)
    * [Lección #6 - Configurar entorno de trabajo (MacOS)](#lesson06)
    * [Lección #7 - Crear tu primer servidor web](#lesson07)
* [Conceptos Fundamentales](#module02)
    * [Lección #8 - Cómo funciona un servidor web](#lesson08)
    * [Lección #9 - Servidor de archivos estáticos](#lesson09)
    * [Lección #10 - Enviar JSON](#lesson10)
    * [Lección #11 - JSON Viewer para Chrome](#lesson11)
    * [Lección #12 - Nodemon para recargar el servidor](#lesson12)
    * [Lección #13 - Rutas POST y Postman](#lesson13)
* [Bases de Datos y CRUDs](#module03)
    * [Lección #14 - Generar app con Express](#lesson14)
    * [Lección #15 - Recorrido de una app generada por el Express Generator](#lesson15)
    * [Lección #16 - Configurar la base de datos](#lesson16)
    * [Lección #17 - Modelos en Mongoose](#lesson17)
    * [Lección #18 - REST y Verbos HTTP](#lesson18)
    * [Lección #19 - Crear lugares](#lesson19)
    * [Lección #20 - Mostrar lugares](#lesson20)
    * [Lección #21 - Mostrar un lugar](#lesson21)
    * [Lección #22 - Actualizar lugar](#lesson22)
    * [Lección #23 - Eliminar lugares](#lesson23)
* [Más allá de los básico](#module04)
    * [Lección #24 - Crear rutas](#lesson24)
    * [Lección #25 - Controladores](#lesson25)
    * [Lección #26 - Paginación](#lesson26)
    * [Lección #27 - Qué es un Middleware en Express](#lesson27)
    * [Lección #28 - Middleware para búsqueda individual](#lesson28)
    * [Lección #29 - Configurar cloudinary](#lesson29)
    * [Lección #30 - Definir archivo con secretos](#lesson30)
    * [Lección #31 - Subir imágenes a cloud](#lesson31)
    * [Lección #32 - Subir imágenes pt 2](#lesson32)
    * [Lección #33 - Mover imágenes a la nube](#lesson33)
    * [Lección #34 - Guardar información de las imágenes en la base de datos](#lesson34)
    * [Lección #35 - Generar SEO URLs](#lesson35)
    * [Lección #36 - Evitar colisiones con las SEO URIs](#lesson36)
    * [Lección #37 - Cambiar búsqueda de ID a Slug](#lesson37)
    * [Lección #38 - Manejo de parámetros](#lesson38)
* [Autenticación de usuarios](#module05)
    * [Lección #39 - Modelo de usuarios](#lesson39)
    * [Lección #40 - Encriptar contraseñas con bcrypt](#lesson40)
    * [Lección #41 - Crear usuarios](#lesson41)
    * [Lección #42 - Generar Json Web Tokens](#lesson42)
    * [Lección #43 - Iniciar sesión](#lesson43)
    * [Lección #44 - Asignar al primer usuario permisos de administrador](#lesson44)
* [Relaciones básicas uno a muchos](#module06)
    * [Lección #45 - Agregar campo usuario a un lugar](#lesson45)
    * [Lección #46 - Proteger rutas con autenticación](#lesson46)
    * [Lección #47 - Crear lugares con un usuario](#lesson47)
    * [Lección #48 - Virtuales para obtener los lugares que creé](#lesson48)
    * [Lección #49 - Proteger rutas para usuarios propietarios](#lesson49)
* [Relaciones muchos a muchos](#module07)
    * [Lección #50 - Crear modelo Favoritos](#lesson50)
    * [Lección #51 - CRUD Favoritos](#lesson51)
    * [Lección #52 - Virtuales para relaciones muchos a muchos](#lesson52)
    * [Lección #53 - Crear modelo de Visitas](#lesson53)
    * [Lección #54 - Enums](#lesson54)
    * [Lección #55 - CRUD de Visitas](#lesson55)
    * [Lección #56 - Visitas a través de negocios](#lesson56)
    * [Lección #57 - Visitas de un negocio](#lesson57)
    * [Lección #58 - Visitas de un usuario](#lesson58)
* [Seguridad en servicios web](#module08)
    * [Lección #59 - Modelo Aplicaciones (Clientes)](#lesson59)
    * [Lección #60 - Asignar valores random a secret y applicationId](#lesson60)
    * [Lección #61 - Crear aplicaciones](#lesson61)
    * [Lección #62 - Eliminar aplicaciones](#lesson62)
    * [Lección #63 - Rutas exclusivas de administradores](#lesson63)
    * [Lección #64 - Proteger rutas con secretos](#lesson64)
    * [Lección #65 - Preflight en CORs](#lesson65)
    * [Lección #66 - Proteger rutas con ApplicationId](#lesson66)
    * [Lección #67 - Encabezados para CORs](#lesson67)
    * [Lección #68 - Dominios enlazados para una aplicación](#lesson68)
    * [Lección #69 - Proteger de dominios no autorizados](#lesson69)

## <a name="module01"></a> Introducción

### <a name="lesson01"></a> Lección #1 - Introducción al proyecto del curso
- - -
Introducción de la aplicación "places" el cuál será desarrollado con NodeJS usando express a nivel de Back-end y React en el Front-End.

### <a name="lesson02"></a> Lección #2 - Requisitos previos para tomar este curso
- - -
Este curso asume que tienes conocimiento profundo de JavaScript, mismo que puedes validar y estudiar en el Curso Profesional de JavaScript https://codigofacilito.com/cursos/javascript-profesional/

Asumimos, además, bases en desarrollo web con tecnologías como HTML, CSS y JavaScript. Puedes repasar las lecciones del curso de Mi Primera Página Web https://codigofacilito.com/cursos/crear-pagina-web y el curso de Desarrollo Web Frontend https://codigofacilito.com/cursos/diseno-web-frontend-2016

Conocimientos básicos de NodeJS pueden ser útiles pero no son necesarios, así mismo, haber trabajado con bases de datos te ayudará a entender de manera más sencilla la comunicación con MongoDB, que es el gestor que usamos en este curso, si no tienes experiencia con bases de datos, puedes tomar el curso gratuito de MongoDB: https://codigofacilito.com/cursos/mongodb

Recuerda también que, siempre que tengas alguna duda, puedes dejarla en la sección de comunidad y el tutor del curso podrá responderla o alguno de tus compañeros.

### <a name="lesson03"></a> Lección #3 - Qué cubriremos en este curso
- - -
Este curso te enseña a desarrollar servicios web, profesionalmente, con NodeJS, aunque no ahondamos en NodeJS mismo, te enseñamos a desarrollar un proyecto completo con esta tecnología.

Cubrimos, también, el uso práctico de Express, cómo organizar y estructurar tu proyecto, a dividir la funcionalidad de tu plataforma en distintos archivos, mismos que te ayudarán a tener una idea más clara de dónde está cada cosa, dentro del proyecto.

Cubrimos mejores prácticas de seguridad, como autenticación por secretos y por IDs de aplicación públicos (muy convenientes para clientes que usan AJAX para consumir el servicio web), además vemos autenticación de usuarios usando tokens, específicamente, usamos el estándar JWT (JSON Web Tokens) para que los clientes de tu servicio web, puedan hacer operaciones en nombre de algún usuario, que previamente inició sesión.

El curso, además, cubre varias partes de Mongoose, un ORM para comunicar MongoDB con NodeJS. Vemos cómo definir relaciones, la estructura de un documento, terminología, virtuals, métodos estáticos y de instancia, virtuales, paginación, urls SEO y mucho más.

Aprenderás, también, cómo mejorar tu código, ahorrar líneas, reciclar, modularizar y mucho más.

### <a name="lesson04"></a> Lección #4 - Descripción del proyecto
- - -
Este curso forma parte del proyecto para crear una Single Page Application con React https://codigofacilito.com/proyectos/single-page-application-con-react-y-nodejs

A partir de este curso, los proyectos en CódigoFacilito son más completos y complejos, para que sigas aprendiendo con prácticas reales que se acercan a lo que la industria demanda, y que te dan los conocimientos que necesitas para desenvolverte en un puesto de trabajo.

Para mejorar la experiencia de aprendizaje y que puedas enfocarte en aquello que quieres o necesitas aprender, los proyectos ahora se dividen en múltiples cursos. Cada curso es independiente y puede ser visto sin la necesidad de ver el resto de los cursos del proyecto, tal es el caso de este curso.

En general, el proyecto se trata de una aplicación para registrar Negocios, Restaurantes, Empresas, etc. Los usuarios se registran para poder hacer comentarios sobre dichos eventos, además de registrar sus propios negocios, definir cuáles son sus favoritos y explorar otros negocios.

En este curso desarrollaremos el servicio web para procesar, almacenar y enviar la información del proyecto, luego de terminar este curso, se asume que el proyecto podría desarrollarse como una app nativa, una app híbrida, una single page app, o lo que sea, el servicio web podrá ser consumido por cualquier tecnología.

Dicho esto, la continuación del proyecto incluye consumir este servicio web usando React.

A continuación puedes ver un vídeo demo del proyecto en funcionamiento.

### <a name="lesson05"></a> Lección #5 - Configurar entorno de trabajo (Windows)
- - -
Para este curso necesitamos usar:
* Una navegador web (Firefox o Google Chrome).
* Un editor de texto (VSCode) o un IDE (Como WebStorm).
* NodeJS en su versión LTS (Junto con NPM).
* Un sistema control de versiones como Git (opcional).

### <a name="lesson06"></a> Lección #6 - Configurar entorno de trabajo (MacOS)
- - -
Para este curso necesitamos usar:
* Una navegador web (Firefox o Google Chrome).
* Un editor de texto (VSCode) o un IDE (Como WebStorm).
* NodeJS en su versión LTS (Junto con NPM).
* Un sistema control de versiones como Git (opcional).

### <a name="lesson07"></a> Lección #7 - Crear tu primer servidor web
- - -
Dentro de la carpeta de nuestro proyecto crearemos un archivo app.js y ejecutaremos el comando:

```sh
$ npm install express --save
```

Para ejecutar nuestro programa lo hacemos de la siguiente manera:

```sh
$ node app.js
```

Podemos probarlo en localhost:3000

## <a name="module02"></a> Conceptos Fundamentales

### <a name="lesson08"></a> Lección #8 - Cómo funciona un servidor web
- - -
Un servidor web, es un programa, instalado generalmente en una computadora especializada, que se encarga de responder las peticiones que recibe vía el protocolo HTTP; generalmente (pero no siempre) de parte de un navegador web.

Generalmente, el principal trabajo de un servidor web es construir y enviar páginas web en formato HTML, aunque además, también envía archivos de utilidad como imágenes, hojas de estilo, archivos javaScript, PDF’s, entre otros.

De manera simple, el flujo de funcionamiento de un servidor web es el siguiente:
1. El usuario solicita un archivo a través de un cliente, que puede ser un navegador web.
2. El navegador web construye una solicitud para dicho archivo, y la envía a través del protocolo HTTP a la computadora.
3. La petición llega a la computadora que contiene el programa, a esta computadora también se le conoce como el servidor.
4. La computadora, a través del programa (servidor web) genera un archivo de respuesta para la petición correspondiente, este archivo se envía, de nuevo a través del protocolo HTTP, de vuelta al cliente que hizo la solicitud.
5. El cliente recibe el archivo respuesta, y se lo muestra al usuario.

Cabe mencionar que el cliente genera una nueva petición por cada archivo que desea solicitar, si una página contiene 2 imágenes, 1 archivo CSS y 1 de JavaScript, hará 4 solicitudes al programa servidor.

El programa servidor contiene lógica que le permite definir qué archivo enviará, si ese archivo requiere de información de una base de datos, si dicha información debe ser procesada o transformada, etc.

En un servidor web, este programa se encarga de enviar y recibir datos, estos datos, además, deben ser enviados en un formato previamente definido.

### <a name="lesson09"></a> Lección #9 - Servidor de archivos estáticos
- - -
Son los archivos que no cambian en el tiempo sin importar las peticiones que reciba. Estas pueden ser imágenes, fuentes, archivos .css y .js, etc.

En esta lección subimos una imágen llamada flanders.png. Para poder acceder a esta imágen, debemos usarla en nuestro archivo app.js, reiniciar el servidor e ir a la ruta localhost:3000/flanders.png.

También podemos probarlo con el archivo index.html que creamos para esta lección.

### <a name="lesson10"></a> Lección #10 - Enviar JSON
- - -
En el desarrollo de servicios es un estándar enviar la información en formato JSON.

_Da igual si ponemos el nombre de la propiedad en comillas dobles o no, por defecto se enviará con comillas dobles._

A diferencia del método send() de la lección pasada es que no estamos enviando un simple string sino un objeto JSON y el navegador lo interpreta como tal.

Lo que haremos en esta lección es un Mocking. Un Mocking es un objeto que simula al comportamiento de un objeto real de una forma controlada. En este ejemplo, los sitios de la aplicación "places". Esta práctica es bastante útil cuando estamos empezando con el desarrollo de una aplicación porque nos permite enviar las estructuras que queremos sin tener todo configurado para generar estas estructuras de manera dinámica.

### <a name="lesson11"></a> Lección #11 - JSON Viewer para Chrome
- - -
Instalación del plugin JSON Viewer para Chrome.

### <a name="lesson12"></a> Lección #12 - Nodemon para recargar el servidor
- - -
Nodemon es un paquete que nos sirve para refrescar el servidor cuando existan cambios en este. Se instala con el comando:

```sh
$ npm install -g nodemon
```

Una vez instalado solamente debemos ejecutar:

```sh
$ nodemon app.js
[nodemon] 2.0.2
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json  
[nodemon] starting `node app.js`
```

El \*.\* significa que se encargará de ver si existen cambios en toda la carpeta sin importar que extensión tenga.

### <a name="lesson13"></a> Lección #13 - Rutas POST y Postman
- - -
En esta lección usaremos el verbo POST y Postman. Para poder leer los valores que se envíen en el cuerpo de la petición necesitamos la siguiente librería:

```sh
$ npm install body-parser --save
```

Podemos ver más sobre body-parser en el siguiente [link](https://github.com/expressjs/body-parser). En Postman crearemos una colección donde iremos almacenando todas nuestras peticiones. En este caso, haremos una prueba con el verbo POST a la ruta localhost:3000/ y mandaremos el objeto JSON que querramos en la opción body -> raw. El ejemplo es el siguiente:

```sh
{
	"nombre": "Eduardo"
}
```

Si todo fue exitoso, debemos ver la salida el mismo objeto JSON que enviamos.

## <a name="module03"></a> Bases de Datos y CRUDs

### <a name="lesson14"></a> Lección #14 - Generar app con Express
- - -
Preparar el entorno de trabajo de una aplicación más robusta es tedioso de configurar al inicio. Es por ello, que existen herramientas que nos permiten automatizar el proceso. Una de ellas es el express generator que se instala de la siguiente manera:

```sh
$ npm i -g express-generator
```

Ahora ejecutamos:

```sh
$ express places
```

Como podemos observar, se nos creó toda la estructura de trabajo de nuestra aplicación.

**IMPORTANTE: Vamos a refactorizar este proyecto, para ello lo que habíamos hecho lo moveremos a una carpeta llamada ejemplos-basicos-de-express y nuestro espacio de trabajo será la estructura generada por el express-generator**

### <a name="lesson15"></a> Lección #15 - Recorrido de una app generada por el Express Generator
- - -
En esta lección vamos a configurar nuestro proyecto para que se adapte a nuestras necesidades. En primer lugar vamos a eliminar la carpeta views/

Configuraremos nuestro app.js para que no use las vistas de jade y cambiar el manejador de errores que ya no sea un render sino un json que retorne los errores correspondientes.

Otra cosa que eliminaremos es el uso de cookie-parser, este sirve para leer las cookies (Las cookies son pedacitos de datos que nosotros solemos guardar) que vienen del navegador. Estas no son necesarias para el flujo de una aplicación de servicio web.

Un servicio web es **stateless**, es decir, que no conserva un estado y una de las estrategias para almacenar el estado de una sesión es usando cookies o la sesiones mismas. Es por ello que no las usaremos.

No usaremos body-parser, las nuevas versiones de express ya lo traen de forma implicita, pero también somos libres de usarlo como hicimos en lecciones pasadas.

Ahora modificaremos el package.json para usar nodemon y quitar los paquetes que no usaremos. Por último, ejecutaremos:

```sh
$ npm run start
```

### <a name="lesson16"></a> Lección #16 - Configurar la base de datos
- - -
Debemos tener instalado MongoDB e iniciar el servidor abriendo el cmd y ejecutando:

```sh
mongod
```

Y debemos tener instalado la librería:

```sh
$ npm install mongoose --save
```

Para las configuraciones vamos a crear archivos que serán usados específicamente para configurar cosas. Será uno para la base de datos, otro para los datos sensibles y vamos a tener otro para el servidor donde vamos a estar subiendo las imágenes.

En esta lección vamos a crear una carpeta config donde tendremos la configuración para conectarnos a la base de datos de MongoDB.

### <a name="lesson17"></a> Lección #17 - Modelos en Mongoose
- - -
Creación de un modelo para poder interactuar con la base de datos (parecido a como se hacía con sequelize pero con MongoDB).

El estándar para crear los modelos de estas lecciones es usar PascalCase en singular, tanto para nombrar los archivos .js como para nombrar los modelos.

### <a name="lesson18"></a> Lección #18 - REST y Verbos HTTP
- - -
REST es un tipo de arquitectura para el desarrollo de proyectos web. Principalmente, los principios que componen esta arquitectura son los siguientes:

#### Recursos identificables

Los recursos en una aplicación web REST, tienen un identificador persistente, es decir, que no cambia con el tiempo. Usualmente este identificador es una URI, como por ejemplo: http://codigofacilito.com/cursos/javascript-profesional identifica el curso de JavaScript.

#### Recursos manipulables con verbos Http.

REST propone que los recursos sean manipulados usando los verbos estándar que forman parte del protocolo HTTP, los usados son los siguientes: GET, POST, PUT, PATCH y DELETE.

GET, por su parte, debe ser usado para leer recursos, y debe ser idempotente, lo que significa que si realizas la misma acción muchas veces, el resultado es el mismo.

Lo importante del párrafo anterior, es entender que GET no modifica, crea o altera los recursos que se almacenan en el servidor web, ya que rompería la propiedad de idempotencia.

Los verbos HTTP se usan de la siguiente manera:

* POST: Sirve para crear nuevos recursos, por ejemplo POST /cursos crea un nuevo curso en la colección de cursos.
* GET: Sirve para lectura de recursos, por ejemplo GET /cursos lee la colección de cursos.
* PUT: Sirve para actualizar recursos, por ejemplo PUT /cursos/javascript-profesional actualizaría el curso de JavaScript Profesional con los datos indicados.
* DELETE: Sirve para eliminar recursos, por ejemplo DELETE /cursos/javascript-profesional eliminaría el recurso indicado.

La ventaja de usar verbos HTTP es que son un estándar, lo que permite desarrollar servicios que recibirán comunicación de otros programas, sin que de antemano estos sepan cómo deben comunicarse.

#### Los recursos se representan de distintas maneras.

Un recurso puede ser representado por un archivo HTML, por una estructura JSON, una estructura XML, un PDF, etc.

La representación depende de la petición, por ejemplo GET /cursos/javascript-profesional.json nos entrega la representación en JSON, mientras que GET /cursos/javascript-profesional entrega la versión en HTML.

#### Los recursos conservan su propio estado

Una de las principales características de un programa REST, es que no tiene un estado. En desarrollo web, el estado puede ser guardado usando sesiones, las sesiones en un servicio web REST no existen.

Usualmente es el cliente el que conserva el estado y lo envía en cada petición.

Este es el trasfondo del funcionamiento de la autenticación con tokens, misma que cubriremos más adelante en el curso.

Para saber más. Existen algunas otras características de una arquitectura REST, sin embargo, no las cubriremos para el propósito de este curso, si quieres saber más, te dejo algunos links interesantes:

http://web.archive.org/web/20130116005443/http://tomayko.com/writings/rest-to-my-wife

https://stackoverflow.com/questions/671118/what-exactly-is-restful-programming

### <a name="lesson19"></a> Lección #19 - Crear lugares
- - -
Para esta lección haremos un store a la base de datos.

### <a name="lesson20"></a> Lección #20 - Mostrar lugares
- - -
Para esta lección mostraremos los lugares almacenados de la base de datos.

### <a name="lesson21"></a> Lección #21 - Mostrar un lugar
- - -
Para esta lección mostraremos un lugar almacenado de la base de datos. En un futuro aplicaremos las SEO URLs.

### <a name="lesson22"></a> Lección #22 - Actualizar lugar
- - -
Para esta lección actualizaremos un lugar almacenado de la base de datos. El método **update()** recibe dos parámetros y **es muy importante saber que si no colocamos nada en el primer parámetro, actualizará todos los registros de la base de datos**.

La diferencia entre los dos métodos propuestos en la lección es que la primera forma ejecuta los hooks que existen en el módelo mientras que el segundo no.

_Nota: Un hook es una función que se puede ejecutar antes o después que ocurra una acción determinada en el modelo._

En esta lección crearemos un mecanismo que nos permite verificar cuales campos son los que deseamos modificar y dejar los otros con el antiguo valor en caso de que no sean necesarios editar.

Podemos darnos cuenta que el mensaje por defecto del método update() es el siguiente:

```sh
{
    "n": 1,
    "nModified": 1,
    "ok": 1
}
```

El cual no es muy descriptivo, es por eso que podemos usar mejor los métodos **findOneAndUpdate()** o **findByIdAndUpdate()**. 

En total vimos 4 formas de actualizar un registro en esta lección pero la mejor forma es tener la [documentación de mongoose](https://mongoosejs.com/docs/queries.html) a la mano para hacer lo que deseemos.

### <a name="lesson23"></a> Lección #23 - Eliminar lugares
- - -
Para esta lección haremos un delete a la base de datos.

## <a name="module04"></a> Más allá de lo básico

### <a name="lesson24"></a> Lección #24 - Crear rutas
- - -
En esta lección vamos a crear un archivo routes/places.js donde administraremos las rutas de nuestro módulo de lugares con el objetivo de ir modularizando nuestro proyecto.

### <a name="lesson25"></a> Lección #25 - Controladores
- - -
En esta lección vamos modularizar nuestra lógica de las rutas al folder llamado controllers. En este caso usan el siguiente estándar de nombres (pero uno puede definir su propio estándar):
* Las rutas van en camelCase y en plural, en este caso places.js
* Los modelos van en PascalCase y en singular, en este caso Place.js
* Los controlladores van en PascalCase con la terminación Controller, en este caso PlacesController.js

### <a name="lesson26"></a> Lección #26 - Paginación
- - -
En esta lección, crearemos una paginación para el método index de places. Para ello debemos instalar el siguiente paquete:

```sh
$ npm install mongoose-paginate --save
```

Debemos configurar nuestro modelo de Places y por último usar el método paginate en el controlador el cual recibe 3 parámetros o **2 si estamos usando promesas** y consiste lo siguiente:
1. El primer parámetro es un objeto JSON con los parámetros de búsqueda.
2. El segundo parámetro es un objeto JSON con las opciones de la paginación.

La documentación del uso de esta librería lo podemos ver de forma más detallada en el siguiente [link](https://www.npmjs.com/package/mongoose-paginate).

### <a name="lesson27"></a> Lección #27 - Qué es un Middleware en Express
- - -
El concepto de middlewares es popular en muchos frameworks, de Desarrollo Web. Están por ejemplo los que dependen fuertemente del concepto como por ejemplo Express, los que lo usan detrás de cámaras o como una configuración avanzada, como lo hace Ruby on Rails, etc.

Un Middleware tiene como propósito tomar dos piezas de la aplicación y conectarlas, como un puente en el que fluye la información. Normalmente decimos que una rutina de código tiene como propósito recibir información y retornarla transformada, la única característica especial de un Middleware es que la información la obtiene de otra función de código para luego enviársela a una función distinta más.

Los middlewares en Express se montan por múltiples razones, una de ellas por ejemplo es validar la información antes de que llegue a la rutina que enviará respuesta hacia el cliente, también pueden usarse para hacer una consulta y guardar información antes de que pase a las funciones que responderán.

Un middleware en Express es una función, cuyo único distintivo es que recibe 3 argumentos:

```sh
function (req, res, next) {  }
```

Los primeros dos argumentos, como cualquier función que responde respuestas del cliente contiene la información de la solicitud en el primer argumento Request, y el objeto Response como segundo argumento, que nos sirve para modificar la respuesta que se enviará a el usuario.

El tercer argumento es muy importante, este es el que distingue un middleware de una función de respuesta. Este tercer argumento es una función que contiene el siguiente middleware o función a ejecutar luego de que el actual termine su ejecución.

Esta función next termina la ejecución de nuestro middleware y puede hacerlo de dos formas.
1. Con éxito, la función next en este caso no recibe argumentos al ejecutarse, indicándole al siguiente punto de la ejecución que todo salió bien.
2. Con un error, el error se envía como argumento de la función, indicando al siguiente punto de la ejecución que algo salió mal y no puede continuar con la respuesta de la petición del cliente.

Todo salió bien:

```sh
function miMiddleware (req, res, next) {
   next();
}
```

Enviamos un error en el middleware:

```sh
function miMiddleware (req, res, next) {
  if (user.permisos != "admin") {
    next(new Error('No tienes permisos para estar aquí'));
  }
}
```

Estas funciones se montan en el proceso de respuesta a una petición usando el método use del objeto app

```sh
const express = require('express');
const app = express();

function miMiddleware (req, res, next) {
   next();
}

app.use(next) //Esto indica que antes de cualquier función de respuesta se debe ejecutar este middleware
```

O bien como parte de la respuesta de una ruta:

```sh
const express = require('express');
const app = express();

function miMiddleware(req, res, next) {
   next();
}

app.get('/', miMiddleware, function (req, res) {
   /* Se ejecutará esta función luego del middleware */
});
```

En ambos casos, es posible que podamos colocar cuantos middlewares queramos definir, lo importante es que cada uno llame la función next, sin argumentos, para que el siguiente middleware se ejecute hasta llegar a la función de respuesta.

Ahora, pasemos al siguiente tema donde veremos cómo integrar este conocimiento en nuestro proyecto.

### <a name="lesson28"></a> Lección #28 - Middleware para búsqueda individual
- - -
El objetivo de esta lección es crear un Middleware que se va a encargar de las búsquedas de los métodos show, update y destroy del controlador. Además, con esta nueva refactorización, prescindiremos de las distintas formas planteadas en el update para actualizar un registro:

```sh
function update(req, res) {
  // Forma #1
  // Place.findById(req.params.id).then(doc => {
  //   doc.title = req.body.title;
  //   doc.description = req.body.description;

  //   doc.save();
  // }).catch(err => {
  //   console.log(err);
  //   res.json(err);
  // });

  /**
   * Este algoritmo lo que hace es verificar si estos atributos existen en el
   * req.body. Los atributos que si existan, serán almacenados en un objeto
   * json placeParams que usaremos para el método update().
   */
  let attributes = [
    "title",
    "description",
    "acceptsCreditCard",
    "openHour",
    "closeHour",
  ];
  let placeParams = {};

  attributes.forEach((attr) => {
    if (Object.prototype.hasOwnProperty.call(req.body, attr)) {
      // En un objeto JSON, req.body.title === req.body['title']
      placeParams[attr] = req.body[attr];
    }
  });

  // Forma #2
  // Place.update({ _id: req.params.id }, placeParams).then(doc => {
  // Place.findOneAndUpdate({ _id: req.params.id }, placeParams, { new: true }).then(doc => {
  Place.findByIdAndUpdate({ _id: req.params.id }, placeParams, { new: true })
    .then((doc) => {
      res.json(doc);
    })
    .catch((err) => {
      console.log(err);
      res.json(err);
    });
}
```

E incluso podemos prescindir del algoritmo que construimos en lecciones anteriores porque las claves que den undefined en Object.assing(), como no existen no se envían al objeto target. Es por eso que las propiedades que no enviemos en el objeto JSON de la petición de actualización se van a ignorar a la hora de trasladar las operaciones a req.place.

**El problema de hacer esto anterior, es que un usuario malicioso puede pasar más propiedades e irlas seteando a la hora de actualizar o crear un registro. Por eso se considera inseguro pasar de forma directo el req.body**

Es por ello que seguiremos usando nuestro antiguo algoritmo.

### <a name="lesson29"></a> Lección #29 - Configurar cloudinary
- - -
La ventaja de usar un proveedor cloud para la subida de imágenes y videos son muchísimas. Podemos destacar la velocidad de descargas como su principal características (por usar réplicas en distintas partes del mundo, por ejemplo). Además que se encargan de manipular las imágenes, generar los thumbnails, etc.

Cloudinary provee un servicio gratuito y necesitamos registrarnos para seguir avanzando con el curso.

### <a name="lesson30"></a> Lección #30 - Definir archivo con secretos
- - -
Configuración del API Key y del API Secrets de Cloudinary. Se recomienda que para un manejador de versiones de git no se suba esta configuración a la versión y todos estos archivos que contengan información sensible se trabajen mejor con variables de entorno (un archivo .env). También haremos uso de la librería dotenv:

```sh
$ npm install dotenv --save
```

### <a name="lesson31"></a> Lección #31 - Subir imágenes a cloud
- - -
Crearemos un modelo que se encargará de la subida de imágenes a cloudinary. Para ello, debemos instalar la librería:

```sh
$ npm install cloudinary --save
```

_Recuerda que un modelo no corresponde necesariamente a una tabla o a una colección en nuestra base de datos._

### <a name="lesson32"></a> Lección #32 - Subir imágenes pt 2
- - -
Para que podamos leer los archivos que vienen de la petición del cliente a express, necesitamos otro middleware con una configuración distinta (muy parecida a bodyParser). Existe una librería que se llama multer y lo podemos instalar con el comando:

```sh
$ npm install multer --save
```

Podemos ver más sobre sus métodos en el siguiente [línk](https://github.com/expressjs/multer).

Para hacer pruebas usamos Postman en la opción Body - form-data.

### <a name="lesson33"></a> Lección #33 - Mover imágenes a la nube
- - -
En esta lección vamos a mover nuestro archivo que creamos de forma local en uploads a cloudinary. Es importante saber que req.files es un objeto que llena multer cuando ejecutamos su función.

En la siguiente lección guardaremos la referencia de la dirección del avatar en la base de datos para poder consultarlo a futuro y subir el avatar y el cover al mismo tiempo.

### <a name="lesson34"></a> Lección #34 - Guardar información de las imágenes en la base de datos
- - -
Uno de los principios del MVC es "Modelos gordos, controladores delgados". Es por ello que mucha de la lógica que se encarga de almacenar la información de los lugares lo moveremos a los modelos.

El código es complejo de entender por el dinamismo que tiene, ya que ahora se encarga de guardar tanto el avatar como el cover.

### <a name="lesson35"></a> Lección #35 - Generar SEO URLs
- - -
En esta lección, generaremos un slug y haremos uso de los hooks en mongoose.

### <a name="lesson36"></a> Lección #36 - Evitar colisiones con las SEO URIs
- - -
Creación de una serie de métodos en el modelo con el objetivo de que no se repita el slug.

### <a name="lesson37"></a> Lección #37 - Cambiar búsqueda de ID a slug
- - -
Búsqueda de lugares por slug y corrección de un bug de la lección pasada.

### <a name="lesson38"></a> Lección #38 - Manejo de parámetros
- - -
Creación de un archivo helpers que usaremos para llamar a funciones que podemos usar en cada uno de nuestros controladores.

## <a name="module05"></a> Autenticación de usuarios

### <a name="lesson39"></a> Lección #39 - Modelo de usuarios
- - -
Creación del modelo de usuarios con mongoose.

### <a name="lesson40"></a> Lección #40 - Encriptar contraseñas con bcrypt
- - -
Para ello, necesitamos instalar la librería con:

```sh
$ npm install mongoose-bcrypt --save
```

La documentación la podemos ver en el siguiente [línk](https://www.npmjs.com/package/mongoose-bcrypt), no es muy difícil su uso.

### <a name="lesson41"></a> Lección #41 - Crear usuarios
- - -
En esta lección seguiremos la misma estructura planteada para registrar un usuario. Podemos comprobar que la librería que instalamos en la lección pasada de forma automática agrega el campo password al modelo y encripta la contraseña si esta es recibida... Por lo cual simplifica mucho su uso.

### <a name="lesson42"></a> Lección #42 - Generar Json Web Tokens
- - -
Primero vamos a instalar la librería jsonwebtoken con el comando:

```sh
$ npm install jsonwebtoken --save
```

Los pasos que debemos realizar son los siguientes:
1. Generar los tokens.
2. Enviarlos.
3. Proteger las rutas con un middleware para validar que estamos recibiendo un jwt válido.

### <a name="lesson43"></a> Lección #43 - Iniciar sesión
- - -
En esta lección vamos a realizar el inicio de sesión.

Una de las cosas que debemos hacer es respetar el principio REST. Perfectamente podríamos haberlo hecho en la ruta y en el controlador users pero en realidad estamos **creando** una sesión y es por eso que creamos una ruta sessions para lograr este objetivo. Una vez más podemos ver las bondades de la librería mongoose-bcrypt al otorgarnos métodos para verificar la contraseña facilitandonos muchísimo la labor.

### <a name="lesson44"></a> Lección #44 - Asignar al primer usuario permisos de administrador
- - -
El primer usuario que se cree en la base de datos tendrá las credenciales de super administrador y es lo que haremos en esta lección. Pero no se recomienda dejarlo mucho tiempo de esta forma ya que muchos conocen esta práctica y el sistema se puede volver vulnerable. Lo que se suele hacer es crearse, asignar a otro usuario el super administrador y ese último creado le revoca los permisos al primer super administrador.

Cuando se almacena el primer usuario en Postman nos dirá admin: false. En realidad si está en true porque imprime el mensaje antes de que se ejecute el hook y eso lo podemos comprobar con nuestra base de datos o iniciando sesión con dicho usuario.

## <a name="module06"></a> Relaciones básicas uno a muchos

### <a name="lesson45"></a> Lección #45 - Agregar campo usuario a un lugar
- - -
En esta lección lo que queremos hacer es una relación uno a muchos de usuarios con lugares, es decir, un usuario puede crear y le pertenece muchos lugares y un lugar es de un usuario en específico.

### <a name="lesson46"></a> Lección #46 - Proteger rutas con autenticación
- - -
En esta lección vamos a leer el jwt enviado por el cliente para saber que usuario está tratandi de autenticarse y para ello usaremos la librería:

```sh
$ npm install express-jwt --save
```

Lo que hace esta librería es verificar si el jwt es válido y si lo es, guarda un req.user para usarlo en el sistema. Caso contrario, bloquea el flujo de las peticiones y manda un error de autenticación.

Podemos ver más información de la librería en el siguiente [línk](https://www.npmjs.com/package/express-jwt).

### <a name="lesson47"></a> Lección #47 - Crear lugares con un usuario
- - -
En esta lección vamos a apoyarnos de las jwt para almacenar el campo _user del modelo de places.

### <a name="lesson48"></a> Lección #48 - Virtuales para obtener los lugares que creé
- - -
En bases de datos no relacionales es mejor hacer una consulta extra a hacer un join o guardar en el mismo registro los documentos que le pertenecen a un registro. Lo que haremos en esta lección es como obtener todos los lugares que le pertenece a un usuario. Existe una forma sencilla de hacerlo pero lo haremos con virtuales.

Los virtuales son atributos virtuales de un documento que tiene como objetivo devolver un atributo adicional cuando se realice una consulta, en este ejemplo, todos los lugares creados por un usuario.

Solo crearemos el virtual y verificar que funcione... Ya en futuras lecciones le asignaremos una ruta y veremos su mejor forma de tratarlo.

### <a name="lesson49"></a> Lección #49 - Proteger rutas para usuarios propietarios
- - -
En esta lección, protegeremos las rutas para que unicamente los usuarios dueños de los lugares puedan editarlos.

## <a name="module07"></a> Relaciones muchos a muchos

### <a name="lesson50"></a> Lección #50 - Crear modelo Favoritos
- - -
Creación de una relación muchos a muchos entre usuarios y lugares, usando un modelo intermedio llamado Favoritos.

### <a name="lesson51"></a> Lección #51 - CRUD Favoritos
- - -
En esta lección haremos un CRUD de favoritos con todas las lecciones vistas con anterioridad.

### <a name="lesson52"></a> Lección #52 - Virtuales para relaciones muchos a muchos
- - -
Crearemos un virtual para el modelo de User que nos retornará los lugares favoritos del usuario.

Además, implementaremos el middleware jwt a una ruta específica del tipo GET.

El ejercicio propuesto de Uriel Hernández en esta lección es hacer un Index pero con los usuarios que tienen como lugar favorito a uno en específico. Es decir, La montaña puede ser el sitio favorito de Pepe, Maria y José.

Algo interesante que nos topamos al crear el virtual en el modelo de Place es que se daña nuestro index de los lugares favoritos por usuario. **¿Porqué pasa esto?** Resulta ser que para consultar los places de un user, User debe hacer required de Place y para consultar los users de un place, Place debe hacer required de User. **Es un bucle** si nos ponemos a ver.

**La solución a este problema** es que sólo debemos colocar los requireds de los modelos al final del documento, después de module.exports; lo demás funciona normal. Asi funcionaría esos requireds de forma local.

### <a name="lesson53"></a> Lección #53 - Crear modelo de Visitas
- - -
Es el otro modelo que va a tener una relación muchos a muchos.

### <a name="lesson54"></a> Lección #54 - Enums
- - -
También conocidas como variables enumeradas y son un tipo de variables que tienen la propiedad de que su rango de valores es un conjunto de constantes. Si no asignamos uno de estos valores, fallará la inserción por una validación, es por ello que solo podemos escoger una de esas opciones del arreglo definido.

### <a name="lesson55"></a> Lección #55 - CRUD de Visitas
- - -
En esta lección, haremos el create y el destroy de visitas.

### <a name="lesson56"></a> Lección #56 - Visitas a través de negocios
- - -
La razón por la cuál no hicimos el index es porque lo haremos de dos formas distintas. La primera será con el endpoint /visits y lo que mostrará son las visitas de un usuario a a ciertos lugares y el otro endpoint será /places/:place-slug/visits y mostrará los usuarios que fueron a este lugar.

En esta lección construiremos las rutas y los controladores. Aún no está operativo ya que falta crear los virtuales.

### <a name="lesson57"></a> Lección #57 - Visitas de un negocio
- - -
En esta lección, dejaremos funcionando el endpoint /places/:place-slug/visits.

### <a name="lesson58"></a> Lección #58 - Visitas de un usuario
- - -
En esta lección, dejaremos funcionando el endpoint /visits. Lo único malo de estas lecciones es que no me dice los datos del lugar que visitamos, como su nombre por ejemplo. Para ello debemos realizar unas consultas adicionales posiblemente en los virtuals.

## <a name="module08"></a> Seguridad en servicios web

### <a name="lesson59"></a> Lección #59 - Modelo Aplicaciones (Clientes)
- - -
Creación del modelo de aplicaciones que será usado para que los clientes se enlacen a nuestra aplicación.

### <a name="lesson60"></a> Lección #60 - Asignar valores random a secret y applicationId
- - -
Para asignar valores aleatorios a los campos applicationId y secret necesitamos la siguiente librería:

```sh
$ npm install randomstring
```

### <a name="lesson61"></a> Lección #61 - Crear aplicaciones
- - -
Creación de una aplicación, podemos ver que se está creando de forma correcta los randomstrings para el applicationId y secret.

### <a name="lesson62"></a> Lección #62 - Eliminar aplicaciones
- - -
Debemos usar otro middleware que desarrollaremos en la siguiente lección ya que este no nos permite eliminar aplicaciones. Por ahora si se pueden eliminar aplicaciones.

### <a name="lesson63"></a> Lección #63 - Rutas exclusivas de administradores
- - -
En esta lección, crearemos un middleware que será usado para que unicamente los administradores creen y eliminen aplicaciones.

Además, reutilizaremos el middleware findUser para refactorizar código en el FavoritesController

### <a name="lesson64"></a> Lección #64 - Proteger rutas con secretos
- - -
**El objetivo de todo este módulo es crear un modelo donde vamos a ir guardando todas las aplicaciones que pueden ser capaz de consumir estas APIs que desarrollamos**. Una vez que ya hicimos las aplicaciones, vamos a utilizarlas. 

El middleware se accionará apenas nosotros creemos nuestra primera aplicación en la base de datos. Ahora para usar cada uno de nuestros endpoints elaborados en Postman debemos pasar en el header el secret correspondiente.

Este req.headers.secret funciona unicamente para aquellos casos o entornos en el que el código no es público, porque precisamente es un secreto. Para los casos en el que el código es público como AJAX, la estrategia es ligeramente distinta y es lo que veremos en los siguientes videos.

### <a name="lesson65"></a> Lección #65 - Preflight en CORs
- - -
Para esta lección, debemos crear una página para simular una petición AJAX y para ello podemos usar la librería node-static:

```sh
$ npm install -g node-static
```

El archivo testAjax/index.html lo agarramos del repositorio del curso.

Para usar la librería node-static, abrimos nuestra consola y ejecutamos el siguiente comando:

```sh
$ static testAjax/index.html
```

Podemos observar en la página de nuestro navegador entrando a la ruta indicada en la consola y dandole a la tecla F12 entrando al partado red en la opción XHR que tendremos un error 500. Por otro lado, en el lado servidor tendremos el error del middleware _An application is required to consume this APIs_.
Esto significa que debemos enviarle un secret y eso lo haremos modificando el método fetch agregando un json de opciones donde agregaremos el application en el header de la siguiente manera:

```sh
fetch("http://localhost:3000/places", {
  method: "GET",
  headers: {
    application: "B1gWzyO2q5dJ45FMDNl3",
  },
}).then(console.log, console.log);
```

Podemos visualizar ahora que en las cabeceras dice que el método de la petición es del tipo OPTIONS. Eso es parte del comportamiento específico que tiene AJAX respecto de como consume un servicio web.

Existe un concepto al cual llamamos Cross-Origins Resource (O Request) por sus siglas CORs que agrupa todo este conocimiento que nosotros necesitamos integrar para poder crear un servicio web que sea consumible a través de AJAX.

Algunas peticiones y eso porque el navegador lo decide, envían antes de enviar la petición que nosotros les indicamos que hicieran (en este caso GET) un Preflight. Este Preflight es una petición OPTIONS hacia el servidor que si se retorna de manera erronea no se continua con la siguiente petición que fue la que nosotros solicitamos y si se contesta con un status 200 se continua con la petición.

Nuestra aplicación debe recibir estas peticion OPTIONS sin autenticarlas y para ello debemos modificar nuestro proyecto en los middlewares. Necesitamos la siguiente librería:

```sh
$ npm install express-unless
```

Y por último, configurar la app para acepte esta petición.

Es importante saber que a pesar de que ya estamos obteniendo un estatus 200, aún tenemos este error:
**Solicitud de origen cruzado bloqueada: La misma política de origen no permite la lectura de recursos remotos en http://localhost:3000/places. (Razón: Solicitud CORS sin éxito).** Esto lo corregiremos en la siguiente lección.

### <a name="lesson66"></a> Lección #66 - Proteger rutas con ApplicationId
- - -
En esta lección, crearemos un middleware que nos sirve para para proteger las rutas sino tenemos el applicationId. 

**¿Cuál es la diferencia entre ApplicationId y el Secret?** Porque técnicamente si yo publico el código con el ApplicationId, alguien podría tomarlo y leerlo de mi código porque es público y utilizarlo para consumir el servicio web a mi nombre... Eso generaría una brecha de seguridad.

El secreto de la protección con el ApplicationId está en el CORs que veremos en la siguiente lección.

### <a name="lesson67"></a> Lección #67 - Encabezados para CORs
- - -
Como vimos en la lección #65 aún no estamos haciendo bien la consulta, vamos a corregir eso en esta lección.

No debemos preocuparnos por las consultas AJAX que estamos realizando en index.html, eso lo haremos mejor en el curso de React. Nótese que mis datos se encuentran como tal en el código y si alguien los toma podría hacerse pasar por mi y usar la aplicación. En la siguiente lección vamos a evitar eso.

### <a name="lesson68"></a> Lección #68 - Dominios enlazados para una aplicación
- - -
Si recordamos de lecciones anteriores, creamos un campo que se llama origins en el modelo de aplicaciones que no le estabamos dando uso. Origins sirve cuando vayamos a la autorización de la aplicación, unicamente se permitan peticiones AJAX desde las rutas especificadas en origins.

Supongamos que tengamos el siguiente registro en la colección de aplicaciones en la base de datos:

```sh
{
    "_id": {
        "$oid": "5e9d0cf5200fdc33e8271a7a"
    },
    "origins": "http://codigofacilito.com, http://127.0.0.1:8080",
    "name": "Mi app de codigofacilito",
    "applicationId": "2TJJv5017qCILTujhRD1",
    "secret": "ShP4pTMz5RnPekYKlpgh",
    "__v": {
        "$numberInt": "0"
    }
}
```

Si nosotros llegasemos a usar el applicationId "2TJJv5017qCILTujhRD1" y no estamos en los dominios especificados, el navegador web nos arrojará un error de este estilo:

**Solicitud desde otro origen bloqueada: la política de mismo origen impide leer el recurso remoto en http://localhost:3000/places (razón: la cabecera CORS 'Access-Control-Allow-Origin' no coincide con 'http://codigofacilito.com').**

La idea detrás de todo esto es que otra aplicación no puedo consumir nuestros servicios salvo las aplicaciones que les dimos accesos con el ApplicationId. Por ahora a pesar de que se está mostrando el mensaje de error si se está haciendo la inserción que será algo que corregiremos en la lección siguiente.

### <a name="lesson69"></a> Lección #69 - Proteger de dominios no autorizados
- - -
En esta lección, vamos a corregir el bug de la lección pasada ya que si la **Solicitud desde otro origen bloqueada** se activa, no debería almacenar los lugares.