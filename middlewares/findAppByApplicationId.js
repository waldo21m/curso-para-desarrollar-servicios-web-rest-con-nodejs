const Application = require("../models/Application");

module.exports = function (req, res, next) {
  if (req.application) return next();

  const applicationId = req.headers.application;

  if (!applicationId) return next();

  Application.findOne({ applicationId })
    .then((app) => {
      if (!app) {
        console.log("Invalid application");
        return next(new Error("Invalid application"));
      }

      req.application = app;

      req.validRequest = req.application.origins.split(",").find(origin => {
        // Expressión regular del espacio en blanco, con esto eliminamos los espacios en blanco
        origin = origin.replace(/\s/g, '');
        console.log(req.headers.origin);
        return origin == req.headers.origin;
      });

      next();
    })
    .catch((error) => {
      next(error);
    });
};
