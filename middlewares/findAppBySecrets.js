const Application = require("../models/Application");

module.exports = function (req, res, next) {
  // Verificar si esta petición es AJAX
  if (req.xhr) return next();

  const secret = req.headers.secret;
  if (!secret) return next();

  Application.findOne({ secret })
    .then((app) => {
      if (!app) {
        console.log("Invalid application");
        return next(new Error("Invalid application"));
      }

      req.application = app;
      req.validRequest = true;
      next();
    })
    .catch((error) => {
      next(error);
    });
};
