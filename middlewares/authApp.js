const Application = require("../models/Application");

module.exports = function () {
  let authApp = function (req, res, next) {
    Application.count({})
      .then((appCount) => {
        if (appCount > 0 && !req.application) {
          console.log("An application is required to consume this APIs");
          return next(
            new Error("An application is required to consume this APIs")
          );
        }

        if (!appCount) {
          req.validRequest = true;
        }

        if (!req.validRequest) {
          console.log("Origin invalid");
          return next(new Error("Origin invalid"));
        }
        next();
      })
      .catch(next);
  };

  authApp.unless = require("express-unless");

  return authApp;
};
