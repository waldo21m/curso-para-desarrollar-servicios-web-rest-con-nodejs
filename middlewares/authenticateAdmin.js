module.exports = function (req, res, next) {
  if (req.fullUser && req.fullUser.admin) return next();

  console.log("You have not permissions to be here");
  next(new Error("You have not permissions to be here"));
};
