const User = require("../models/User");

module.exports = function (req, res, next) {
  if (req.user) {
    User.findById(req.user.id).then((user) => {
      req.fullUser = user;
      next();
    });
  } else {
    /**
     * Si dejamos este next fuera del if se ejecutará primero... Recuerda que
     * las promesas demoran en responder y la ejecución de código continuará. 
     */
    next();
  }
};
